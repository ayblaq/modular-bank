import "./App.css";
import { Contact } from "../container";

function App() {
  return (
    <div className="body-container">
      <div>
        <Contact></Contact>
      </div>
    </div>
  );
}

export default App;
