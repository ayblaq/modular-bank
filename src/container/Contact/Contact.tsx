import React from "react";
import { ContactAddress, ContactForm } from "../../modular/components"
import { Container, Grid, Header } from "semantic-ui-react"
import './Contact.css'

const Contact = () => {
    return (
        <Container>
            <Grid divided='vertically' container stackable>
                <Grid.Row columns={2}>
                    <Grid.Column width={6}>
                        <ContactAddress></ContactAddress>
                    </Grid.Column>
                    <Grid.Column width={10} className={'contact-form'}>
                        <Header as='h1' className={'h1-contact'}>Contact us</Header>
                        <ContactForm></ContactForm>
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </Container>
    )
}

export default Contact;