import { Modal, Header } from 'semantic-ui-react'
import './MessageBox.css'

interface messagebox {
  open: boolean,
  message: string,
  title: string,
  close: any
}

const MessageBox = (props: messagebox) => {
    const { open, message, title, close } = props

    return (
        <Modal
        basic
        onClose={close}
        open={open}
        size='small'>
        <Header className={"title"}>
          {title}
        </Header>
        <Modal.Content>
          <p className={"message"}>
                {message}
          </p>
        </Modal.Content>
      </Modal>
    )
}

export default MessageBox