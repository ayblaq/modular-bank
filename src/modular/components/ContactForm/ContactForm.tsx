import React, { useState, useEffect } from "react";
import { Form, Grid, Button } from 'semantic-ui-react'
import { industryApi, operationApi, countryApi } from "../../../api/api"
import { MessageBox } from "../../components"
import * as EmailValidator from 'email-validator';
import './ContactForm.css'

const ContactForm = () => {

    const defaultInput = {
        firstname: "",
        lastname: "",
        email: "",
        job: "",
        company: "",
        country: "N/A",
        operation: "N/A",
        industry: "Banking",
        comment: "",
        policy: false,
        newsletter: false
    }

    const [formInput, setData] = useState({...defaultInput})

    const [status, setStatus] = useState({ submit: false })

    const [modal, setModal] = useState({
        show: false
    });

    useEffect(() => {
        validateInput()
    }, [formInput])

    const handleInputChange = (e: React.ChangeEvent<any>) => {
        e.preventDefault()
        setData({ ...formInput, [e.target.name]: e.target.value })
    }

    const handleSelectChange = (e: React.ChangeEvent<any>, data: any) => {
        e.preventDefault()
        setData({ ...formInput, [data.name]: data.value })
    }

    const handleCheckBox = (e: React.ChangeEvent<any>, data:any) => {
        e.preventDefault()
        setData({ ...formInput, [data.name]: data.checked})
    }

    const validateInput = () => {
        if (formInput.firstname.length > 0 && formInput.industry.length > 0 &&
            formInput.company.length > 0 && formInput.country.length > 0 && formInput.country !== "N/A"
            && formInput.policy && EmailValidator.validate(formInput.email)
        ) {
            setStatus({ submit: true })
        } else {
            if (status.submit === true) {
                setStatus({ submit: false })
            }
        }
    }

    const submitForm = () => {
        //perform submission actions here
        setModal({show: true})
    }

    const closeModal = () => {
        setModal({show: false})
        setData({...defaultInput})
    }

    const message = "Thank you for your interest. We will contact you in 1-2 working days. You can close this form by clicking outside this box."

    return (
        <div>
            <Form>
                <Form.Group widths={'equal'}>
                    <Form.Input name="firstname" value={formInput.firstname} label='First name' onChange={handleInputChange} required></Form.Input>
                    <Form.Input name="lastname" value={formInput.lastname} label='Last name' onChange={handleInputChange} ></Form.Input>
                </Form.Group>
                <Form.Group widths={'equal'}>
                    <Form.Input type="email" name="email" value={formInput.email} label='Email' onChange={handleInputChange} required></Form.Input>
                    <Form.Input name="job" value={formInput.job} label='Job title' onChange={handleInputChange}></Form.Input>
                </Form.Group>
                <Form.Group widths={'equal'} className={'form-break'}>
                    <Form.Input name="company" value={formInput.company} label='Company' onChange={handleInputChange} required></Form.Input>
                    <Form.Select name="industry" label='Industry' value={formInput.industry} options={industryApi} onChange={handleSelectChange} required></Form.Select>
                </Form.Group>
                <Form.Group widths={'equal'}>
                    <Form.Select name="country" label='Country' value={formInput.country} options={countryApi} onChange={handleSelectChange} required></Form.Select>
                    <Form.Select name="operation" label='Operating geography' value={formInput.operation} options={operationApi} onChange={handleSelectChange}></Form.Select>
                </Form.Group>
                <Form.TextArea label='What would you like to talk about?' name="comment" value={formInput.comment} onChange={handleInputChange} className={'form-break'} />
                <Grid>
                    <Grid.Row columns={2}>
                        <Grid.Column floated='left'>
                            <Form.Checkbox label={{ children:<p> By submitting this form I accept  <a className="contact-form-link" href="https://modularbank.co/privacy-policy" target="_blank" rel="noreferrer"> privacy policy and cookie policy.</a> </p>}} name="policy" checked={formInput.policy} onChange={handleCheckBox}/>
                            <Form.Checkbox label='I would like to receive your newsletter.' name="newsletter"  onChange={handleCheckBox} />
                        </Grid.Column>
                        <Grid.Column floated='right'>
                            <Button type="submit" className={status.submit ? "contact-send allow" : "contact-send"} onClick={submitForm}>Send</Button>
                        </Grid.Column>
                    </Grid.Row>
                </Grid>
            </Form>
             <MessageBox title="All good!" message={message} open={modal.show} close={closeModal}></MessageBox>
        </div>
    )
}

export default ContactForm;
