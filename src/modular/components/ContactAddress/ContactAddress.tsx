import React from "react";
import {
    Container
} from 'semantic-ui-react';
import './ContactAddress.css'


const ContactAddress = () => {
    return (
        <Container className={"addr-container"}>
            <div>
                <div>Media enquiries:</div>
                <a className={"primary-link"} href="mailto:careers@modularbank.co">press@modularbank.co</a>
            </div>
            <div>
                <div>Career questions:</div>
                <a className={"primary-link"} href="mailto:careers@modularbank.co">careers@modularbank.co</a>
            </div>
            <div>
                <div>Our offices:</div>
                <p>Tallinn, Estonia
                    <br />Vabaduse Workland
                    <br />Pärnu mnt 12, 10146
                </p>
            </div>
            <div>
                <p>Berlin, Germany
                    <br />Bikini Berlin, Scaling Spaces, 2.OG
                    <br />Budapester Str.46
                    <br />10787 Berlin
                </p>
            </div>
        </Container>
    )
}

export default ContactAddress;